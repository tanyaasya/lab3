#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <termios.h>


using namespace std;

bool sendSome(int to, char* data, size_t size)
{
    int bytes_send = 0;
    while(bytes_send != size)
    {
        bytes_send = send(to, data, size, 0);
        if (bytes_send == -1)
        {
            printf("Error %d:%s\n" ,errno,strerror(errno));

        }
        cout << "Отправлено: " << bytes_send << " bytes" << endl;
    }
    return true;
}

bool receiveSome(int from, /*const*/ char* data, size_t size)
{
    int received = 0;
    int bytes_received = 0;

    cout << "Нужно принять: " << size << endl << endl;

    while (bytes_received != size)
    {
        received = recv(from, /*(char*)*/data, size-bytes_received, 0);
        if (received == -1)
        {
            printf("Error %d:%s\n" ,errno,strerror(errno));
        }
        bytes_received += received;
        cout << "Принято: " << bytes_received << endl;
        cout << "Осталось допринять: " << size - bytes_received << endl;
    }
    return true;
}

int main()
{
    cout << "Клиент!" << endl;


    int channel = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    sockaddr_in server;

    server.sin_family = AF_INET;

    cout << "Введите IP сервера: ";
    string address;
    cin >> address;
    server.sin_addr.s_addr =  inet_addr(address.c_str());


    cout << "Введите порт: ";
    unsigned short port;
    cin >> port;
    server.sin_port = htons(port);

    int result = connect(channel, (const sockaddr*)(&server), sizeof(server));

    if (result != 0)
    {
        cout << "Соединение не установлено!\n";
        return -1;
    }

    bool connected = true;
    int key = 0;
    string str;
    while (connected)
    {
        key = 0;
        cout.clear();
        cin.sync();
        cout << "\nВведите\n"
             << "1 - Отправить запрос\n"
             << "2 - Получить ответ\n"
             << "3 - Прервать соединение\n";

                cin >> key;
        cout << endl;


        switch(key)
        {
        case 1:
        {
            cout.clear();
            cin.sync();

            cout << "Введите имя файла или /quit: ";
            cin>>str;
                        //getline(cin, str);

            char msgType = '\0';
            if (strcmp(str.data(), "/q") == 0)
            {
                msgType = 'Q';
            }
            else
            {
                msgType = 'R';
            }

            vector<char> fileName(str.begin(), str.end());

            //size_t sizeforsending = data.size();
            uint64_t sizeForSending = fileName.size();
            //cout << "Str size is: " << sizeForSending << endl;

            if (sendSome(channel, (char*)&sizeForSending, sizeof(sizeForSending)))
            {
                if(sendSome(channel, &msgType, sizeof(msgType)))
                {
                    if (msgType == 'Q')
                    {
                        close(channel);
                        return 0;
                    }
                    if(sendSome(channel, fileName.data(), sizeForSending))
                    {
                        cout << "Имя файла передано\n";
                    }
                    else
                    {
                        cout << "Ошибка передачи имени файла\n";
                    }
                }
                else
                {
                    cout << "Ошибка передачи типа\n";
                }
            }
            else
            {
                cout << "Ошибка передачи размера\n";
            }
            break;
        }

        case 2:
        {
            uint64_t msgSize = 0;

            if (receiveSome(channel, (char*)&msgSize, sizeof(msgSize)))
            {
                char msgType = '\0';

                if (receiveSome(channel, &msgType, sizeof(msgType)))
                {
                    cout << "Тип пакета: " <<  msgType << endl;
                    vector<char> msg;
                    msg.resize(msgSize+1, '\0');

                    if (receiveSome(channel, msg.data(), msgSize))
                    {
                        if (msgType = 'F')
                        {
                            cout << "Содержимое файла:\n";
                            cout << msg.data() << endl;
                                break;
                            }

                    }
                }
            }
            break;
        }

        case 3:
            {
                close(channel);
                return 0;
            }
        default:
            cout << "Ошибка!\n";
            break;
        }
    }
    return 0;
}